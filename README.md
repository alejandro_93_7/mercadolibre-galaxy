# Pronostico Vulcano

## Enunciado:
En una galaxia lejana, existen tres civilizaciones. Vulcanos, Ferengis y Betasoides. Cada civilización vive en paz en su respectivo planeta.
Dominan la predicción del clima mediante un complejo sistema informático.
A continuación el diagrama del sistema solar.

#Premisas:
- El planeta Ferengi se desplaza con una velocidad angular de 1 grados/día en sentido horario. Su distancia con respecto al sol es de 500Km.
- El planeta Betasoide se desplaza con una velocidad angular de 3 grados/día en sentido horario. Su distancia con respecto al sol es de 2000Km.
- El planeta Vulcano se desplaza con una velocidad angular de 5 grados/día en sentido anti­horario, su distancia con respecto al sol es de 1000Km.
- Todas las órbitas son circulares. Cuando los tres planetas están alineados entre sí y a su vez alineados con respecto al sol, el sistema solar experimenta un período de sequía.
Cuando los tres planetas no están alineados, forman entre sí un triángulo. Es sabido que en el momento en el que el sol se encuentra dentro del triángulo, el sistema solar experimenta un período de lluvia, teniendo éste, un pico de intensidad cuando el perímetro del triángulo está en su máximo.
Las condiciones óptimas de presión y temperatura se dan cuando los tres planetas están alineados entre sí pero no están alineados con el sol.

Realizar un programa informático para poder predecir en los próximos 10 años:
1. ¿Cuántos períodos de sequía habrá?
2. ¿Cuántos períodos de lluvia habrá y qué día será el pico máximo de lluvia?
3. ¿Cuántos períodos de condiciones óptimas de presión y temperatura habrá?

Bonus:
Para poder utilizar el sistema como un servicio a las otras civilizaciones, los Vulcanos requieren tener una base de datos con las condiciones meteorológicas de todos los días y brindar una API REST de consulta sobre las condiciones de un día en particular.
1. Generar un modelo de datos con las condiciones de todos los días hasta 10 años en adelante utilizando un job para calcularlas.
2. Generar una API REST la cual devuelve en formato JSON la condición climática del día consultado.
3. Hostear el modelo de datos y la API REST en un cloud computing libre (como APP Engine o Cloudfoudry) y enviar la URL para consulta:
Ej: GET → http://….../clima?dia=566 → Respuesta: {“dia”:566, “clima”:”lluvia”}

## Tecnologías utilizadas:
- Java spring
- H2 database (localhost:PUERTO/h2-console)
- CloudFoundry (https://galaxyweather-alejandromacula-deployment-uniqueuser-777.cfapps.io)


## Aclaraciones:
- Para toda la galaxia se toma como definición que un año tiene 360 días que es el tiempo que tarda el planeta mas lento en dar la vuelta completa. En este punto todos los planetas vuelven a estar alineados en su posición inicial (día 0).
- Se asume que los días que no apliquen para las condiciones especificadas son tipo "soleado".
- Por cuestiones de rendimiento este sistema está optimizado para esta galaxia y su funcionamiento en particular.
- Se toma como día de tormenta unicamente el mayor perímetro que puede tomar el triángulo de los 3 planetas, por lo que la cantidad de días de tormenta se basa en la cantidad de veces que se repite ese perimetro.

## Funcionamiento
Al iniciar la aplicación ejecuta la función init que se encarga de inicializar la base de datos almacenando los pronosticos correspondientes a 360 días. Para cualquier consulta de un día en particular el sistema busca el día equivalente en el año inicial y le asigna ese clima, esto mismo hace para el resto de proyecciones ya que el clima se repite todos los años.


## API

- Clima de un día en particular

	https://galaxyweather-alejandromacula-deployment-uniqueuser-777.cfapps.io/clima?dia=566
	
	{"clima":"lluvia","dia":566}

- Cantidad de días de un período en particular

	https://galaxyweather-alejandromacula-deployment-uniqueuser-777.cfapps.io/periodo?tipo={sequia, lluvia, optimo, tormenta, soleado}&años=1
	
	{"clima":"sequia","cantidadDeDias":1}

- Cantidad de días de un período en particular por cada tipo de clima

	https://galaxyweather-alejandromacula-deployment-uniqueuser-777.cfapps.io/periodos?años=1

    [{"clima":"soleado","cantidadDeDias":266},{"clima":"lluvia","cantidadDeDias":89},{"clima":"tormenta","cantidadDeDias":4},{"clima":"temperatura y presion optima","cantidadDeDias":0},{"clima":"sequia","cantidadDeDias":1}]

- Días específicos en los que va a habrá tormenta y su perímetro 

	https://galaxyweather-alejandromacula-deployment-uniqueuser-777.cfapps.io/picoDeLluvia?años=1

    [{"dia":72,"perimetro":6262.300354242003},{"dia":108,"perimetro":6262.300354242003},{"dia":252,"perimetro":6262.300354242003},{"dia":288,"perimetro":6262.300354242003}]

